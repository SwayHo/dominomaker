const dominoIndex = [
    [0],
    [1, 60],
    [2, 21, 98],
    [3, 41, 61, 134],
    [4, 80, 22, 99, 168],
    [5, 117, 42, 62, 135, 200],
    [6, 152, 81, 23, 100, 169, 230],
    [7, 185, 118, 43, 63, 136, 201, 258],
    [8, 216, 153, 82, 24, 101, 170, 231, 284],
    [9, 245, 186, 119, 44, 64, 137, 202, 259, 308],
    [10, 272, 217, 154, 83, 25, 102, 171, 232, 285, 330],
    [11, 297, 246, 187, 120, 45, 65, 138, 203, 260, 309, 350],
    [12, 320, 273, 218, 155, 84, 26, 103, 172, 233, 286, 331, 368],
    [13, 341, 298, 247, 188, 121, 46, 66, 139, 204, 261, 310, 351, 384],
    [14, 360, 321, 274, 219, 156, 85, 27, 104, 173, 234, 287, 332, 369, 398],
    [15, 377, 342, 299, 248, 189, 122, 47, 67, 140, 205, 262, 311, 352, 385, 410],
    [16, 392, 361, 322, 275, 220, 157, 86, 28, 105, 174, 235, 288, 333, 370, 399, 420],
    [17, 405, 378, 343, 300, 249, 190, 123, 48, 68, 141, 206, 263, 312, 353, 386, 411, 428],
    [18, 416, 393, 362, 323, 276, 221, 158, 87, 29, 106, 175, 236, 289, 334, 371, 400, 421, 434],
    [19, 425, 406, 379, 344, 301, 250, 191, 124, 49, 69, 142, 207, 264, 313, 354, 387, 412, 429, 438],
    [20, 432, 417, 394, 363, 324, 277, 222, 159, 88, 30, 107, 176, 237, 290, 335, 372, 401, 422, 435, 440],
    [437, 426, 407, 380, 345, 302, 251, 192, 125, 50, 70, 143, 208, 265, 314, 355, 388, 413, 430, 439],
    [433, 418, 395, 364, 325, 278, 223, 160, 89, 31, 108, 177, 238, 291, 336, 373, 402, 423, 436],
    [427, 408, 381, 346, 303, 252, 193, 126, 51, 71, 144, 209, 266, 315, 356, 389, 414, 431],
    [419, 396, 365, 326, 279, 224, 161, 90, 32, 109, 178, 239, 292, 337, 374, 403, 424],
    [409, 382, 347, 304, 253, 194, 127, 52, 72, 145, 210, 267, 316, 357, 390, 415],
    [397, 366, 327, 280, 225, 162, 91, 33, 110, 179, 240, 293, 338, 375, 404],
    [383, 348, 305, 254, 195, 128, 53, 73, 146, 211, 268, 317, 358, 391],
    [367, 328, 281, 226, 163, 92, 34, 111, 180, 241, 294, 339, 376],
    [349, 306, 255, 196,129, 54, 74, 147, 212, 269, 318, 359],
    [329, 282, 227, 164, 93, 35, 112, 181, 242, 295, 340],
    [307, 256, 197, 130, 55, 75, 148, 213, 270, 319],
    [283, 228, 165, 94, 36, 113, 182, 243, 296],
    [257, 198, 131, 56, 76, 149, 214, 271],
    [229, 166, 95, 37, 114, 183, 244],
    [199, 132, 57, 77, 150, 215],
    [167, 96, 38, 115, 184],
    [133, 58, 78, 151],
    [97, 39, 116],
    [59, 79],
    [40]
]



const MAX_COUNT = 441;
const Scene = require('Scene');
const Time = require('Time');
const TouchGestures = require('TouchGestures');
const timeInMilliseconds = 50;
export const Diagnostics = require('Diagnostics');
const startDomino1 = Scene.root.find('Cube.000');
const startDomino2 = Scene.root.find('Cube.001');
const startDomino3 = Scene.root.find('Cube.060');


let isStarted = false;
let totalTime = 0;
let dominoArray = new Array();

class Domino {
    constructor(obj, maxSlip, slipVelocity){
        this.obj = obj;
        this.rotX = 0;
        this.rotY = 0;
        this.rotZ = 0;
        this.count = 0;
        this.maxSlip = maxSlip;
        this.slipVelocity = slipVelocity
    }

    rotate(){
        this.count += 1;
        if(this.rotX > this.maxSlip){
            this.rotX -= this.slipVelocity;
        }
    }

    startRotate(){
        this.rotX -= 0.9;
    }

    getRotate(){
        return { x : this.rotX, y : this.rotY, z : this.rotZ};
    }

    getObj(){
        return this.obj;
    }
}

for(let i = 0; i < MAX_COUNT; i++){
    let pre = "Cube.";
    if (i < 100){
        pre = "Cube.0";
    }
    if(i < 10){
        pre = "Cube.00";
    }
    dominoArray.push(new Domino(Scene.root.find(pre + i), -1.35, 0.135));
}

function rotateDomino(index){
    if (index > dominoIndex.length - 1){
        return; 
    }
    let arr = dominoIndex[index];
    for(let i=0; i < arr.length; i++){
        dominoArray[arr[i]].rotate();
        let obj = dominoArray[arr[i]].getObj();
        let rot = dominoArray[arr[i]].getRotate();
        obj.transform.rotationX = rot.x;
        Diagnostics.watch("roatate domino index ", arr[i]);
    }
}

function getDominoRotation(index){
    if (index < dominoIndex.length){
        return dominoArray[dominoIndex[index][0]].getRotate();
    }else{
        return null;
    }
}

function countElaspTime(){
    let index = Math.floor(totalTime/200);
    let minRot = -0.4;
    rotateDomino(index);

    rotateDomino(1);
    rotateDomino(2);

    let rot1 = getDominoRotation(index);
    let rot2 = getDominoRotation(index + 1);
    let rot3 = getDominoRotation(index + 2);

    if (rot1 != null){
        if (rot1.x < minRot){
            rotateDomino(index + 1);
        }
    }

    if (rot2 != null){
        if(rot2.x < minRot){
            rotateDomino(index + 2);
        }
    }

    if (rot3 != null){
        if(rot3.x > minRot){
            rotateDomino(index + 3);
        }
    }

    totalTime += timeInMilliseconds;
}

let intervalTimer = null;

TouchGestures.onTap(startDomino1).subscribe(function (gesture) {
    if(intervalTimer == null && !isStarted){
        isStarted = true
        let startIdx = dominoIndex[0][0]
        let obj = dominoArray[startIdx].getObj();
        dominoArray[startIdx].startRotate();
        let rot = dominoArray[startIdx].getRotate();
        obj.transform.rotationX = rot.x;
        intervalTimer = Time.setInterval(countElaspTime, timeInMilliseconds);
    }
})

TouchGestures.onTap(startDomino2).subscribe(function (gesture) {
    if(intervalTimer == null && !isStarted){
        isStarted = true
        let startIdx = dominoIndex[0][0]
        let obj = dominoArray[startIdx].getObj();
        dominoArray[startIdx].startRotate();
        let rot = dominoArray[startIdx].getRotate();
        obj.transform.rotationX = rot.x;
        intervalTimer = Time.setInterval(countElaspTime, timeInMilliseconds);
    }
})

TouchGestures.onTap(startDomino3).subscribe(function (gesture) {
    if(intervalTimer == null && !isStarted){
        isStarted = true
        let startIdx = dominoIndex[0][0]
        let obj = dominoArray[startIdx].getObj();
        dominoArray[startIdx].startRotate();
        let rot = dominoArray[startIdx].getRotate();
        obj.transform.rotationX = rot.x;
        intervalTimer = Time.setInterval(countElaspTime, timeInMilliseconds);
    }
})